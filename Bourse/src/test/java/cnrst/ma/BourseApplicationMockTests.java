package cnrst.ma;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringJUnit4ClassRunner.class)
public class BourseApplicationMockTests {

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void isCorrectLoginMockTest() throws Exception {
		 this.mockMvc.perform(formLogin().user("email","invalid").password("123456"))
         .andExpect(unauthenticated())
         .andExpect(status().is3xxRedirection());
	}

	@Test
	public void isCorrectLoginPasswordMockTest() throws Exception {
		this.mockMvc.perform(formLogin().user("email","cnrst4@gmail.com").password("123456"))
		.andExpect(authenticated()).andReturn();
	}

	@Test
	public void getLoginMockTest() throws Exception {
		this.mockMvc.perform(formLogin().user("email","cnrst4@gmail.com"))
		.andExpect(unauthenticated()).andReturn();
	}
	
	@Test
	public void getLoginMockTest2() throws Exception {
		this.mockMvc.perform(formLogin().user("email","cnrst4@gmail.com"))
		.andExpect(authenticated()).andReturn();
	}
	
}
