package cnrst.ma;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import cnrst.ma.entity.Boursier;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BourseApplicationTests {
	
	private Boursier boursier;
	
	@Before
	public void Initializer() {
		boursier = new Boursier("said" , "password");
	}
	
	@Test
	public void isCorrectLoginTest() {
		String expected = "ahmed";
		assertEquals(false , boursier.isCorrectLogin(expected));
	}
	
	@Test
	public void isCorrectLoginPasswordTest() {
		String loginExpected = "said";
		String passwordExpected = "abcd123";
		assertEquals(false , boursier.isCorrectLoginPassword(loginExpected , passwordExpected));
	}
	
	@Test
	public void getLoginTest() {
		String expected = "said";
		assertEquals(expected, boursier.getLogin());
	}
	
}
